package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
)

const extension = "swf"

var (
	domain string
	secure bool
	_url   string
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	flag.StringVar(
		&domain, "domain", "darkorbit.com",
		"Domain on which get main and preloader")
	flag.BoolVar(&secure, "secure", false, "Force the use of SSL")
	flag.StringVar(&_url, "url", "", "Force use this URL to download the two files (preloader & main)")
	flag.Parse()

	getFile("preloader")
	fmt.Printf("\n")
	getFile("main")
}

func getFile(filename string) {
	// If the directory doesn't exists, create it
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		err = os.Mkdir(filename, 0644)
		check(err)
	}

	filepath := fmt.Sprintf("%s.%s", filename, extension)

	// Create the file (empty)
	file, err := os.Create(filepath)
	check(err)

	/*
	 * Set the HTTP protocol to use
	 * Here, 'http' as default and 'https' if the flag 'secure' is present
	 */
	protocol := "http"
	if secure {
		protocol = "https"
	}

	address := ""
	if _url != "" {
		u, err := url.Parse(_url)
		check(err)
		host, _, _ := net.SplitHostPort(u.Host)
		if host == "" {
			host = u.Host
		}

		address = fmt.Sprintf("%s/%s.swf", _url, filename)
		domain = host
	} else {
		address = fmt.Sprintf("%s://%s/spacemap/%s.swf", protocol, domain, filename)
	}
	response, err := http.Get(address)
	check(err)

	log.Printf("Downloading %s...", address)

	/*
	 * Since the domain name can be set by the user, there's a possibility
	 * that, the specified server doesn't have the preloader.swf and main.swf.
	 * Thus the need to check and end the natural process of this tool if
	 * the given server respond with a 404 error.
	 */
	if response.StatusCode >= 400 {
		log.Printf("%s, skipping ...", response.Status)
		return
	}
	defer response.Body.Close()

	// Using this way to use the response.Body twice
	var buf bytes.Buffer
	res := io.TeeReader(response.Body, &buf)

	hash := sha256.New()

	numBytesWritten, err := io.Copy(file, res)
	check(err)
	file.Close()

	_, err = io.Copy(hash, &buf)
	check(err)

	stringHash := hex.EncodeToString(hash.Sum(nil))
	if stringHash == "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" {
		panic("Something went wrong: The SHA256 hash of the file is null")
	}

	data, err := ioutil.ReadFile(filepath)
	check(err)

	signature := fmt.Sprintf("%s", data[0:3])
	switch signature {
	case "FWS":
		log.Print("This file is neither encrypted nor compressed\n")
	case "CWS":
		log.Print("This file is not encrypted but is compressed\n")
	default:
		signature = "encrypted"
		log.Print("This file is encrypted\n")
	}

	destination := fmt.Sprintf(
		"%s/%s.%s.%s.%s.%s",
		filename, filename, stringHash[0:8], strings.ToLower(signature), domain, extension)
	err = os.Rename(filepath, destination)
	if err != nil {
		log.Print("File already exists. Deleting the duplicate...")
		os.Remove(destination)
		os.Rename(filepath, destination)
	}

	log.Printf("Downloaded %d bytes\n", numBytesWritten)
	log.Printf("SHA256 %s", stringHash)
}
