This Python script is used to dump all in-memory swf files.
It checks for the process with a big amount of reserved space -- supposed to be a viable solution since one of the swf file requested by the main.swf is one of more than 400MB which includes all the images needed for the client.

This script only works on Windows and needs a recent installation of JPEXS Free Flash Decompiler which can be found at this address: https://github.com/jindrapetrik/jpexs-decompiler

It is a work-in-progress!
At the moment, it dump all swf files in the directory it's started on (they are more than 200 files).
