import psutil, os, sys

ffdec_path = 'C:\Program Files (x86)\FFDec\\ffdec.jar'
browsers = ['chrome', 'firefox', 'opera', 'iexplore', 'edge']
process_size = 500

print('Searching for processes that might contain the wanted swf file(s)')
processes = []
for p in psutil.process_iter(attrs=['pid', 'name', 'memory_info']):
    if p.info['memory_info'].rss >= process_size * 1024 * 1024:
        if any(browser in p.info['name'] for browser in browsers):
            processes.append(p.pid)

if not len(processes):
    print('No process found')
    print('Make sure the main client is loaded in an internet browser')
    sys.exit(0)
else:
    print('Found %i: %s' %(len(processes), processes))

print('Searching for swf files in the selected processes...')
for pid in processes:
    print('[+] Launching FFdec to search for swf files in the selected process (%i)' %(pid))
    os.system('java -jar "%s" -memorySearch %s'%(ffdec_path, pid))
