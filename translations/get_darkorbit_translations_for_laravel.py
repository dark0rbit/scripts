import requests, io, os, shutil, re
import xml.dom.minidom as xml

languages = ['bg', 'cs', 'da', 'de', 'el', 'en', 'es', 'fi', 'fr', 'hu', 'it', 'ja', 'nl', 'no', 'pl', 'pt', 'ro', 'ru', 'sk', 'sv', 'tr']
dir = 'lang'

if os.path.exists(dir):
    shutil.rmtree(dir)

def create_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

create_directory(dir)

def prepare_php_trans_file(file):
    if not os.path.exists(file):
        f = open(file, "w+")
        f.write('<?php\n\nreturn [\n')
        f.close()

def write_to_file(file, trans):
    f = open(file, 'a+', encoding='utf8', errors='ignore')
    for s in trans:
        index = s.attributes['name'].value
        if s.childNodes:
            value = s.childNodes[0].nodeValue
        else:
            value = ''
        f.write('    \'' + index + '\' => \'' + value \
                .replace('\\', '\\\\') \
                .replace('\'', '\\\'') \
                .replace('\n', '\\n') + '\',\n')
    f.write('];\n')
    f.close()

def camel_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

def prepare_file(slug, url, word_to_replace):
    for language in languages:
        directory = 'lang/' + language
        create_directory(directory)
        file_name = directory + '/' + camel_case(slug) + '.php'
        prepare_php_trans_file(file_name)

        r = requests.get(url.replace(word_to_replace, language))
        xml_file = io.StringIO(str(r.content, 'utf-8'))
        try:
            parsed_xml = xml.parse(xml_file)
            items = parsed_xml.getElementsByTagName('item')
            write_to_file(file_name, items)
        except Exception as e:
            print(e)

# `lang` is gonna be replaced by the language shortcut
# i.e: `en` or `fr`
url = 'https://darkorbit-22.bpsecure.com/spacemap/templates/lang/'
resources = ['resource_eic', 'resource_inventory', 'resource_achievement', \
    'resource_chat', 'resource_loadingScreen', 'resource_items', 'flashres', 'resource_quest']

for resource in resources:
    prepare_file(resource, url + resource + '.xml', 'lang')
